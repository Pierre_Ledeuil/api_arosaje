# API_Arosaje
Cette API est en Node JS et utilise Express.

# Installation de l'API:
    Assurez vous de bien avoir joué les commandes suivantes:

    npm install
    
    node server.js

# Si la BDD n'existe pas:

    cd /data

    node table.js

    (Cette commande marche aussi en cas de reset de la BDD pour des tests.)

# Postman

    Demandez l'accès à l'admin.