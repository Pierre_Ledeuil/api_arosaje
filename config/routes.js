const express = require('express');
const router = express.Router();

// Import des fonctions de gestion des utilisateurs
const {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
} = require('../controller/utilisateursController');

// Routes pour les utilisateurs
router.get('/utilisateurs', getAllUsers);
router.get('/utilisateurs/:id', getUserById);
router.post('/utilisateurs', createUser);
router.patch('/utilisateurs/:id', updateUser);
router.delete('/utilisateurs/:id', deleteUser);

module.exports = router;
