const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

const logger = require('./logs/logger');
const routes = require('./config/routes');

app.use(bodyParser.json());

// Utilisez les routes
app.use('/', routes);

app.listen(port, () => {
    console.log(`API running on ${port}`);
});
