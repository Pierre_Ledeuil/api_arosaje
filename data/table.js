const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const fs = require('fs');
const logger = require('../logs/logger');

// Chemin vers la base de données SQLite
const dbPath = path.resolve(__dirname, 'Arosaje.db');

// Vérifier si le fichier de base de données existe déjà
if (!fs.existsSync(dbPath)) {
    console.log('La base de données n\'existe pas. Création en cours...');
    fs.closeSync(fs.openSync(dbPath, 'w'));
}

// Création d'une nouvelle instance de base de données SQLite
const db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
        console.error('Erreur lors de la connexion à la base de données :', err.message);
    } else {
        console.log('Connexion à la base de données SQLite réussie.');
    }
});

// Script de création de la table
const createTableUtilisateur = `
    CREATE TABLE IF NOT EXISTS utilisateur (
        id INTEGER PRIMARY KEY,
        nom TEXT NOT NULL,
        email TEXT NOT NULL UNIQUE,
        mot_de_passe TEXT NOT NULL,
        id_role INTEGER,
        FOREIGN KEY (id_role) REFERENCES role(id)
    );
`;
const createTableRole = `
    CREATE TABLE IF NOT EXISTS role (
        id INTEGER PRIMARY KEY,
        nom TEXT NOT NULL
    );
`;
const createTableUtilisateurRole = `
    CREATE TABLE IF NOT EXISTS utilisateur_role (
        utilisateur_id INTEGER,
        role_id INTEGER,
        FOREIGN KEY (utilisateur_id) REFERENCES utilisateur(id),
        FOREIGN KEY (role_id) REFERENCES role(id),
        PRIMARY KEY (utilisateur_id, role_id)
    );
`;
const createTableAnnonce =`
    CREATE TABLE IF NOT EXISTS annonces (
        id INTEGER PRIMARY KEY,
        idUtilisateur INTEGER NOT NULL,
        idListePlantes INTEGER NOT NULL,
        idLocalisation INTEGER NOT NULL,
        titre TEXT NOT NULL,
        description TEXT NOT NULL,
        dateDebut TEXT NOT NULL,
        dateFin TEXT NOT NULL,
        FOREIGN KEY (idUtilisateur) REFERENCES utilisateur(id),
        FOREIGN KEY (idListePlantes) REFERENCES listePlantes(id), -- Assure-toi d'avoir la table listePlantes
        FOREIGN KEY (idLocalisation) REFERENCES localisation(id)  -- Assure-toi d'avoir la table localisation
    );
`;



const insertDefaultDataRole = `
    INSERT INTO role (nom) VALUES ('admin');
`;
const insertDefaultDataUtilisateur = `
    INSERT INTO utilisateur (nom,email,mot_de_passe,id_role) VALUES("admin","admin@admin.com","admin",1);
`;

// Fonction pour exécuter une requête SQL avec des paramètres
function runQuery(sql, params = []) {
    return new Promise((resolve, reject) => {
        db.run(sql, params, function(err) {
            if (err) {
                reject(err.message);
            } else {
                resolve(this.lastID);
            }
        });
    });
}

// Exécution du script de création de la table
async function createTable() {
    try {

        await runQuery(createTableUtilisateur);
        console.log('Table "Utilisateur" créée avec succès.');

        await runQuery(createTableRole);
        console.log('Table "Role" créée avec succès.');

        await runQuery(createTableUtilisateurRole);
        console.log('Table "Utilisateur_Role" créée avec succès.');

        await runQuery(createTableAnnonce);
        console.log('Table "Annonce" créée avec succès.')

        await runQuery(insertDefaultDataRole);
        console.log('Insertion des données par défault "Role" avec succès.');

        await runQuery(insertDefaultDataUtilisateur);
        console.log('Insertion des données par défault "Utilisateur" avec succès.');

    } catch (error) {
        console.error('Erreur lors de la création de la table :', error);
    } finally {
        db.close((err) => {
            if (err) {
                return console.error('Erreur lors de la fermeture de la connexion à la base de données :', err.message);
            }
            console.log('Connexion à la base de données SQLite fermée avec succès.');
        });
    }
}

// Appel de la fonction pour créer la table
createTable();
